//TODO: Write a function that receives two integer matrices and outputs
// the sum of the two matrices. Then in your main() function, input a few
// examples to check your solution. Output the results of your function to 
// cout. You could even write a separate function that prints an arbitrarily 
// sized matric to cout.

#include <iostream>
#include <vector>

using namespace std;

int main(){
    vector <vector<int> > sumMatrices(vector <vector<int> >, vector <vector<int> >);
    vector <vector<int> > matrix1 (5, vector<int>(3, 5));
    vector <vector<int> > matrix2 (5, vector<int>(3, 4));
    
    vector <vector<int> > result;
    
    result = sumMatrices(matrix1, matrix2);
    
    for(int row=0; row<result.size(); row++){
        cout << "[" ;
        for(int col=0; col<result[0].size(); col++){
            cout << result[row][col] << ", ";
        }
        cout << "]";
        cout << endl;
    }
    
    return 0;
}

vector < vector<int> > sumMatrices(vector <vector<int> > matrix1, vector <vector<int> > matrix2){
    
    //vector <vector <int> > result (matrix1.size, vector<int> (matrix1[0].size, 0));
    vector <vector <int> > result (5, vector <int> (3, 0));
    for(int row=0; row<matrix1.size(); row++){
        for(int col=0; col<matrix1[0].size(); col++){
            result[row][col] = matrix1[row][col] + matrix2[row][col];
        }
    }
    return result;
    
}