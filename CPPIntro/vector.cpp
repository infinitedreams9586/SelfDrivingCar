//TODO: Use this as a playground to practice with vectors
#include<iostream>
#include<vector>

using namespace std;

int main(){
    
    vector<int> vector1 (3);
    vector<int> vector2 (3);
    
    vector<int> vector3 (5);
    vector<int> vector4 (5);

    static const int array1[] = {5, 10, 27};
    static const int array2[] = {3, 17, 12};
    
    static const int array3[] = {17, 10, 31, 5, 7};
    static const int array4[] = {3, 1, 6, 19, 8};
    
    int arrSize = sizeof(array1) / sizeof(array1[0]);
    int arrSize2 = sizeof(array3) / sizeof(array3[0]);
    
    for(int i=0; i<arrSize; i++){
        vector1[i] = array1[i];
        vector2[i] = array2[i];
    }
    
    for(int i=0; i<arrSize2; i++){
        vector3[i] = array3[i];
        vector4[i] = array4[i];
    }
    
    // function declaration
    vector<int> vectorOperation(vector<int>, vector<int>, char);
    vector<int> result = vectorOperation(vector1, vector2, '-');
    vector<int> result2 = vectorOperation(vector3, vector4, '*');
    
    for(int i=0; i<result.size(); i++){
        cout << result[i] << endl;
    }
    
    for(int i=0; i<result2.size(); i++){
        cout << result2[i] << endl;
    }
    
    return 0;
}

vector<int> vectorOperation(vector<int> vector1, vector<int> vector2, char operation){
    vector<int> result (vector1.size());
    switch(operation){
        case '+':
            for (int i=0; i<vector1.size(); i++){
                result[i] = vector1[i] + vector2[i];
            }
            break;
        
        case '*':
            for (int i=0; i<vector1.size(); i++){
                result[i] = vector1[i] * vector2[i];
            }
            break;
        
        case '-':
            for (int i=0; i<vector1.size(); i++){
                result[i] = vector1[i] - vector2[i];
            }
            break;
    }
    return result;
}


//TODO:
// Fill out your program's header. The header should contain any necessary
// include statements and also function declarations


//TODO:
// Write your main program. Remember that all C++ programs need
// a main function. The most important part of your program goes
// inside the main function. 