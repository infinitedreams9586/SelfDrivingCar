import math

class BinaryHeap:
    def __init__(self):
        # (priority, item)
        self.heaplist = [(0, 0)]
        self.members = set()

    @property
    def size(self):
        return len(self.heaplist) - 1

    @property
    def list(self):
        return self.heaplist

    def is_member(self, val):
        return val in self.members

    def get_index(self, val):
        for idx, v in enumerate(self.heaplist):
            if v[1] == val:
                return idx
        return 0

    def moveUp(self):
        pos = self.size
        while (pos // 2) > 0:
            if self.heaplist[pos][0] < self.heaplist[pos // 2][0]:
                tmp = self.heaplist[pos // 2]
                self.heaplist[pos // 2] = self.heaplist[pos]
                self.heaplist[pos] = tmp
            pos = pos // 2

    def insert(self, new_val):
        self.heaplist.append(new_val)
        self.members.add(new_val[1])
        self.moveUp()

    def minChildPos(self, pos):
        if (pos * 2) + 1 > self.size:
            return pos * 2
        else:
            if self.heaplist[pos * 2][0] < self.heaplist[pos * 2 + 1][0]:
                return pos * 2
            else:
                return pos * 2 + 1

    def moveDown(self):
        pos = 1
        while (pos * 2) <= self.size:
            min_c = self.minChildPos(pos)
            if self.heaplist[pos][0] > self.heaplist[min_c][0]:
                tmp = self.heaplist[pos]
                self.heaplist[pos] = self.heaplist[min_c]
                self.heaplist[min_c] = tmp
            pos = min_c

    def delete(self):
        ret_val = self.heaplist[1]
        self.heaplist[1] = self.heaplist[self.size]
        self.heaplist.pop()
        self.moveDown()
        return ret_val
    
def distance(M, point1, point2):
    a = M.intersections[point1]
    b = M.intersections[point2]
    return math.sqrt((b[0]-a[0])**2 + (b[1]-a[1])**2)


## Ref. used : http://code.activestate.com/recipes/578919-python-a-pathfinding-with-binary-heap/
## And : https://en.wikipedia.org/wiki/A*_search_algorithm
def shortest_path(M, start, goal):
    frontier = BinaryHeap()
    frontier.insert((0, start))

    came_from = {}
    close_set = set()
    g_score = {}
    f_score = {}

    g_score[start] = 0
    came_from[start] = None

    while frontier.size != 0:
        current = frontier.delete()[1]

        if current == goal:
            data = []
            while current in came_from:
                data.insert(0, current)
                current = came_from[current]
            return data

        close_set.add(current)
        for next in M.roads[current]:
            tentative_gscore = g_score[current] + distance(M, current, next)
            if (next in close_set and tentative_gscore > g_score.get(next, 0)):
                continue

            if (tentative_gscore < g_score.get(next,0)) or ((not frontier.is_member(next))):
                came_from[next] = current
                g_score[next] = tentative_gscore
                f_score[next] = tentative_gscore + distance(M, next, goal)
                frontier.insert((f_score[next], next))

    return False